class Code
  def initialize(pegs)
    raise(ArgumentError) if pegs.class != Array

    @pegs = pegs.map {|peg| peg.downcase}
  end

  attr_reader :pegs

  PEGS = {"r" => "red", "g" => "green", "b" => "blue", "y" => "yellow", "o" => "orange", "p" => "purple"}

  def [](i)
    @pegs[i]
  end

  def ==(obj)
    return false if obj.class != Code || self.pegs != obj.pegs

    true
  end

  def self.random
    pegs = []
    4.times do
      pegs << PEGS.keys.sample
    end

    Code.new(pegs)
  end

  def self.parse(guess)
    input = guess.downcase.split("")

    input.each {|clr| raise "you gotta pick from the colors maaan" if !PEGS.include?(clr)}

    Code.new(input)
  end

  def exact_matches(guess)
    matches = 0
    guess.pegs.each_with_index do |peg, i|
      if peg == self.pegs[i]
        matches += 1
      end
    end

    matches
  end

  def near_matches(guess)
    indexed_guess = guess.pegs.map.with_index {|gpeg, i| [gpeg, i]}
    indexed_self = self.pegs.map.with_index {|speg, j| [speg, j]}
    indexed_self.each_with_index do |spair, j|
      if indexed_guess[j] == spair
        indexed_self.delete(spair)
        indexed_guess.delete(spair)
      end
    end
    near_matches = 0
    indexed_guess.each do |gpair|
      indexed_self.each do |spair|
        if spair[0] == gpair[0] && spair[1] != gpair[1]
          near_matches += 1
          indexed_self.delete(spair)
          break
        end
      end
    end

    near_matches
  end


end



class Game
  def initialize(code = Code.random)
    @secret_code = code
    @guesses = 0
  end


  attr_reader :secret_code, :guesses

  def guesses=(guesses)
    @guesses = guesses
  end

  def get_guess
    puts "gimme a guess"
    guess = gets.strip
    @guesses += 1

    Code.parse(guess)
  end

  def display_matches(guess)
    print "#{@secret_code.exact_matches(guess)} exact matches\n#{@secret_code.near_matches(guess)} near matches\n"
  end




end
